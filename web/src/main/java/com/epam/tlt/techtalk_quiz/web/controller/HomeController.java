package com.epam.tlt.techtalk_quiz.web.controller;

import com.epam.tlt.techtalk_quiz.domain.entity.enums.UserRoleEnum;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
public class HomeController {

    @RequestMapping("/")
    public String index(Model model, Principal principal, HttpServletRequest request) {
        model.addAttribute("userName", principal.getName());
        model.addAttribute("isAdmin", request.isUserInRole(UserRoleEnum.ROLE_ADMIN.name()));
        return "index";
    }
}
