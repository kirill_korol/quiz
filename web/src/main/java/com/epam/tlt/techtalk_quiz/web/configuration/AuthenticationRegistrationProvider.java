package com.epam.tlt.techtalk_quiz.web.configuration;

import com.epam.tlt.techtalk_quiz.domain.entity.UserEntity;
import com.epam.tlt.techtalk_quiz.domain.entity.UserRoleEntity;
import com.epam.tlt.techtalk_quiz.domain.repository.UserRepository;
import com.epam.tlt.techtalk_quiz.domain.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AuthenticationRegistrationProvider implements AuthenticationProvider {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        List<UserRoleEntity> grants;

        if (userRepository.existsByLogin(name)) {
            if (userRepository.existsByLoginAndPassword(name, password)) {
                grants = userRoleRepository.findAllByUser_LoginAndUser_Password(name, password);
            } else {
                return null;
            }
        } else {
            UserEntity user = userRepository.createUser(name, password);
            grants = user.getRoles();
        }
        return new UsernamePasswordAuthenticationToken(name, password, grants);
    }


    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
