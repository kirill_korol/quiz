package com.epam.tlt.techtalk_quiz.importer;

import com.epam.tlt.techtalk_quiz.domain.entity.QuestionEntity;
import com.epam.tlt.techtalk_quiz.domain.repository.QuestionRepository;
import com.epam.tlt.techtalk_quiz.importer.parser.XmlParser;
import com.epam.tlt.techtalk_quiz.importer.reader.FileReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;

@Service
public class OnApplicationStartImporter {

    @Autowired
    QuestionRepository questionRepository;
    @Value("${techtalk-quiz.importer.data-source.file}")
    private String file;
    @Autowired
    private FileReader fileReader;

    @Autowired
    private XmlParser xmlParser;

    @PostConstruct
    void init() {
        Collection<QuestionEntity> questionEntities = xmlParser.parse(fileReader.read(file));
        questionRepository.saveAll(questionEntities);
    }
}
