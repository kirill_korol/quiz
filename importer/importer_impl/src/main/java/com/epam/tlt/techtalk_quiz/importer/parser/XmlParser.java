package com.epam.tlt.techtalk_quiz.importer.parser;

import com.epam.tlt.techtalk_quiz.domain.entity.QuestionAnswerEntity;
import com.epam.tlt.techtalk_quiz.domain.entity.QuestionEntity;
import com.epam.tlt.techtalk_quiz.domain.entity.enums.QuestionModifier;
import com.epam.tlt.techtalk_quiz.domain.entity.enums.QuestionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component(value = "XmlParser")
public class XmlParser implements DataParser {
    Logger LOGGER = LoggerFactory.getLogger(XmlParser.class);

    private static QuestionEntity parseQuestion(Node questionNode) {
        QuestionEntity entity = new QuestionEntity();
        getParamsFromQuestionAttributes(entity, questionNode);

        NodeList questionChild = questionNode.getChildNodes();
        for (int j = 0; j < questionChild.getLength(); j++) {
            Node questionItem = questionChild.item(j);
            if (questionItem.getNodeType() != Node.ELEMENT_NODE)
                continue;

            if (questionItem.getNodeName().equals("title")) {
                entity.setTitle(questionItem.getTextContent());
            }
            if (questionItem.getNodeName().equals("description")) {
                entity.setDescription(combineBody(questionItem).toString());
            }
            if (questionItem.getNodeName().equals("answers")) {
                entity.setQuestionAnswers(getQuestionAnswers(questionItem, entity));
            }
        }
        return entity;
    }

    private static List<QuestionAnswerEntity> getQuestionAnswers(Node questionItem, QuestionEntity entity) {
        List<QuestionAnswerEntity> questionAnswers = new ArrayList<>();
        NodeList answerNodes = questionItem.getChildNodes();

        for (int i = 0; i < answerNodes.getLength(); i++) {
            Node answerNode = answerNodes.item(i);
            if (answerNode.getNodeType() != Node.ELEMENT_NODE)
                continue;

            QuestionAnswerEntity answer = new QuestionAnswerEntity();
            answer.setAnswerText(answerNode.getTextContent());
            NamedNodeMap attributes = answerNode.getAttributes();
            if (attributes != null) {
                answer.setCorrect(Boolean.valueOf(attributes.getNamedItem("correct").getTextContent()));
            }
            answer.setQuestion(entity);
            questionAnswers.add(answer);
        }
        return questionAnswers;
    }

    private static void getParamsFromQuestionAttributes(QuestionEntity entity, Node questionNode) {
        NamedNodeMap attributes = questionNode.getAttributes();
        if (attributes != null) {
            entity.setTimeout(Integer.valueOf(attributes.getNamedItem("timeout").getTextContent()));
            entity.setQuestionType(QuestionType.valueOf(attributes.getNamedItem("type").getTextContent()));
            entity.setQuestionModifier(QuestionModifier.valueOf(attributes.getNamedItem("modifier").getTextContent()));
        }
    }

    private static StringBuilder combineBody(Node questionItem) {
        StringBuilder builder = new StringBuilder();
        NodeList htmlNodes = questionItem.getChildNodes();
        for (int i = 0; i < htmlNodes.getLength(); i++) {
            Node item = htmlNodes.item(i);
            if (item.getNodeType() != Node.ELEMENT_NODE) {
                builder.append(item.getTextContent());
                continue;
            }
            builder.append("<" + item.getNodeName() + ">");
            builder.append(item.getTextContent());
            builder.append("</" + item.getNodeName() + ">");
        }
        return builder;
    }

    @Override
    public Collection<QuestionEntity> parse(byte[] content) {
        List<QuestionEntity> list = new ArrayList<>();

        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.parse(new ByteArrayInputStream(content));

            Node root = document.getDocumentElement();
            NodeList questions = root.getChildNodes();
            for (int i = 0; i < questions.getLength(); i++) {
                Node questionNode = questions.item(i);
                if (questionNode.getNodeType() != Node.ELEMENT_NODE)
                    continue;
                list.add(parseQuestion(questionNode));
            }
        } catch (Exception e) {
            LOGGER.error("error while import question", e);
        }
        return list;
    }
}
