package com.epam.tlt.techtalk_quiz.importer;

import com.epam.tlt.techtalk_quiz.importer.parser.DataParser;
import com.epam.tlt.techtalk_quiz.importer.reader.DataReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:application.yml")
public class ImportModuleConfig {

    @Autowired
    private ApplicationContext context;

    @Bean
    public DataReader DataFileReader(@Value("${techtalk-quiz.importer.service.reader}") String qualifier) {
        return (DataReader) context.getBean(qualifier);
    }

    @Bean
    public DataParser DataXmlParser(@Value("${techtalk-quiz.importer.service.parser}") String qualifier) {
        return (DataParser) context.getBean(qualifier);
    }
}
