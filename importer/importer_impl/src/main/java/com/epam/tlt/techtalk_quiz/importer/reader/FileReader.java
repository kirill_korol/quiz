package com.epam.tlt.techtalk_quiz.importer.reader;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;

@Component(value = "FileReader")
public class FileReader implements DataReader<String> {
    private static Logger log = LoggerFactory.getLogger(FileReader.class);

    public byte[] read(String source) {
        ByteArrayOutputStream out = new ByteArrayOutputStream(500);
        try (InputStream in = new BufferedInputStream(new FileInputStream(source))) {
            int c;
            while ((c = in.read()) != -1) {
                out.write(c);
            }
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Error, file can not read.");
        }
        log.debug("File successfully read.");
        return out.toByteArray();
    }
}
