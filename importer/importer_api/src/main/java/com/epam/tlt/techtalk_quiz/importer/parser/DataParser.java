package com.epam.tlt.techtalk_quiz.importer.parser;

import com.epam.tlt.techtalk_quiz.domain.entity.QuestionEntity;

import java.util.Collection;

public interface DataParser {
    Collection<QuestionEntity> parse(byte[] content);
}
