package com.epam.tlt.techtalk_quiz.importer.reader;

public interface DataReader<T> {
    byte[] read(T source);
}
