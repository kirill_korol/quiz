package com.epam.tlt.techtalk_quiz.workflow.state;

public interface State {
    String doSome(String some);
}
