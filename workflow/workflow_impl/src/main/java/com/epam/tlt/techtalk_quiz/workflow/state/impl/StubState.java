package com.epam.tlt.techtalk_quiz.workflow.state.impl;

import com.epam.tlt.techtalk_quiz.workflow.state.State;

public class StubState implements State {

    public String doSome(String some) {
        return some;
    }
}
