package com.epam.tlt.techtalk_quiz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuizTechtalkApplication {
    private static Logger log = LoggerFactory.getLogger(QuizTechtalkApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(QuizTechtalkApplication.class, args);
    }
}
