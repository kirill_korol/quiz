package com.epam.tlt.techtalk_quiz.domain.repository;

import com.epam.tlt.techtalk_quiz.domain.entity.SomeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SomeRepository extends CrudRepository<SomeEntity, Long> {
}
