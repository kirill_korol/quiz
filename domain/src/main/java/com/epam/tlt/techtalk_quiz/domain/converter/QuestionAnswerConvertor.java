package com.epam.tlt.techtalk_quiz.domain.converter;

import com.epam.tlt.techtalk_quiz.domain.dto.QuestionAnswerDto;
import com.epam.tlt.techtalk_quiz.domain.entity.QuestionAnswerEntity;

public class QuestionAnswerConvertor {

    public static QuestionAnswerDto toDto(QuestionAnswerEntity entity) {
        QuestionAnswerDto dto = new QuestionAnswerDto();
        dto.setId(entity.getId());
        dto.setAnswerText(entity.getAnswerText());
        dto.setQuestionId(QuestionConvertor.toDto(entity.getQuestion()));
        dto.setCorrect(entity.isCorrect());
        return dto;
    }

    public static QuestionAnswerEntity toEntity(QuestionAnswerDto dto) {
        QuestionAnswerEntity entity = new QuestionAnswerEntity();
        entity.setId(dto.getId());
        entity.setAnswerText(dto.getAnswerText());
        entity.setQuestion(QuestionConvertor.toEntity(dto.getQuestionId()));
        entity.setCorrect(dto.isCorrect());
        return entity;
    }
}
