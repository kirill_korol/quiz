package com.epam.tlt.techtalk_quiz.domain.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "question_answer")
public class QuestionAnswerEntity {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "question_id", nullable = false)
    private QuestionEntity question;

    @Column(name = "answerText", length = 1000)
    private String answerText;

    @Column(name = "isCorrect")
    private boolean isCorrect;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "answer")
    private List<UserAnswersEntity> userAnswers;

    @Override
    public String toString() {
        return "Answer{" + "answerText=" + answerText
                + ", isCorrect=" + isCorrect + '}';
    }
}
