package com.epam.tlt.techtalk_quiz.domain.entity.enums;

public enum QuestionType {
    CHECKBOX, RADIO
}
