package com.epam.tlt.techtalk_quiz.domain.dto;

import com.epam.tlt.techtalk_quiz.domain.entity.enums.QuestionModifier;
import com.epam.tlt.techtalk_quiz.domain.entity.enums.QuestionType;
import lombok.Data;

import java.util.List;

@Data
public class QuestionDto {

    private Long id;
    private String title;
    private String description;
    private QuestionType questionType;
    private Integer timeout;
    private QuestionModifier questionModifier;
    private List<QuestionAnswerDto> questionAnswer;
    private List<UserAnswersDto> userAnswers;
}
