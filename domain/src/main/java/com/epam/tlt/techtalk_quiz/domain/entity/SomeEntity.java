package com.epam.tlt.techtalk_quiz.domain.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "some")
public class SomeEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "some")
    private String some;

}
