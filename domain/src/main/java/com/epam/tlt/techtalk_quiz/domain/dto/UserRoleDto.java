package com.epam.tlt.techtalk_quiz.domain.dto;

import lombok.Data;

@Data
public class UserRoleDto {

    private Long id;
    private String role;
    private UserDto user;
}
