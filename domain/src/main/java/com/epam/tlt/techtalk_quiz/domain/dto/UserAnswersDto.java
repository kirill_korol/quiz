package com.epam.tlt.techtalk_quiz.domain.dto;

import lombok.Data;

@Data
public class UserAnswersDto {

    private Long id;
    private QuestionDto question;
    private QuestionAnswerDto answer;
    private UserDto user;
    private Integer bet;
}
