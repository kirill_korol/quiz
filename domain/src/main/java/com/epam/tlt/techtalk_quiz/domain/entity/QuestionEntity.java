package com.epam.tlt.techtalk_quiz.domain.entity;


import com.epam.tlt.techtalk_quiz.domain.entity.enums.QuestionModifier;
import com.epam.tlt.techtalk_quiz.domain.entity.enums.QuestionType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "question")
public class QuestionEntity {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "title", length = 2000)
    private String title;

    @Column(name = "description", length = 4000)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private QuestionType questionType;

    @Column(name = "timeout")
    private Integer timeout;

    @Enumerated(EnumType.STRING)
    @Column(name = "modifier")
    private QuestionModifier questionModifier;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "question", cascade = CascadeType.ALL)
    private List<QuestionAnswerEntity> questionAnswers;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "question")
    private List<UserAnswersEntity> userAnswers;

    @Override
    public String toString() {
        String toString = "Question{" + "type=" + questionType.toString() +
                ", modifier=" + questionModifier.toString() +
                ", timeout=" + timeout +
                ", description='" + description + '\'' +
                '}';
        for (QuestionAnswerEntity i : questionAnswers
                ) {
            toString += i.toString();
        }
        return toString;
    }
}
