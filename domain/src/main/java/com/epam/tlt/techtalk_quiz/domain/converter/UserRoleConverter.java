package com.epam.tlt.techtalk_quiz.domain.converter;

import com.epam.tlt.techtalk_quiz.domain.dto.UserRoleDto;
import com.epam.tlt.techtalk_quiz.domain.entity.UserRoleEntity;

public class UserRoleConverter {

    public static UserRoleDto toDto(UserRoleEntity entity) {
        UserRoleDto dto = new UserRoleDto();
        dto.setId(entity.getId());
        dto.setId(entity.getId());
        dto.setRole(entity.getRole());
        dto.setUser(UserConverter.toDto(entity.getUser()));
        return dto;
    }

    public static UserRoleEntity toEntity(UserRoleDto dto) {
        UserRoleEntity entity = new UserRoleEntity();
        entity.setId(dto.getId());
        entity.setRole(dto.getRole());
        entity.setUser(UserConverter.toEntity(dto.getUser()));
        return entity;
    }
}
