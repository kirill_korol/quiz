package com.epam.tlt.techtalk_quiz.domain.entity.enums;

public enum UserRoleEnum {
    ROLE_ADMIN,
    ROLE_USER
}
