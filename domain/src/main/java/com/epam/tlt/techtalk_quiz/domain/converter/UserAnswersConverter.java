package com.epam.tlt.techtalk_quiz.domain.converter;

import com.epam.tlt.techtalk_quiz.domain.dto.UserAnswersDto;
import com.epam.tlt.techtalk_quiz.domain.entity.UserAnswersEntity;

public class UserAnswersConverter {


    public static UserAnswersDto toDto(UserAnswersEntity entity) {
        UserAnswersDto dto = new UserAnswersDto();
        dto.setId(entity.getId());
        dto.setAnswer(QuestionAnswerConvertor.toDto(entity.getAnswer()));
        dto.setQuestion(QuestionConvertor.toDto(entity.getQuestion()));
        dto.setUser(UserConverter.toDto(entity.getUser()));
        dto.setBet(entity.getBet());
        return dto;
    }

    public static UserAnswersEntity toEntity(UserAnswersDto dto) {
        UserAnswersEntity entity = new UserAnswersEntity();
        entity.setId(dto.getId());
        entity.setAnswer(QuestionAnswerConvertor.toEntity(dto.getAnswer()));
        entity.setQuestion(QuestionConvertor.toEntity(dto.getQuestion()));
        entity.setUser(UserConverter.toEntity(dto.getUser()));
        entity.setBet(dto.getBet());
        return entity;
    }
}
