package com.epam.tlt.techtalk_quiz.domain.converter;


import com.epam.tlt.techtalk_quiz.domain.dto.QuestionAnswerDto;
import com.epam.tlt.techtalk_quiz.domain.dto.QuestionDto;
import com.epam.tlt.techtalk_quiz.domain.dto.UserAnswersDto;
import com.epam.tlt.techtalk_quiz.domain.entity.QuestionAnswerEntity;
import com.epam.tlt.techtalk_quiz.domain.entity.QuestionEntity;
import com.epam.tlt.techtalk_quiz.domain.entity.UserAnswersEntity;

import java.util.List;
import java.util.stream.Collectors;

public class QuestionConvertor {

    public static QuestionDto toDto(QuestionEntity entity) {
        QuestionDto dto = new QuestionDto();
        dto.setId(entity.getId());
        dto.setTitle(entity.getTitle());
        dto.setDescription(entity.getDescription());
        dto.setQuestionType(entity.getQuestionType());
        dto.setQuestionModifier(entity.getQuestionModifier());
        dto.setTimeout(entity.getTimeout());

        List<QuestionAnswerDto> questionDtos = entity.getQuestionAnswers().stream().map(QuestionAnswerConvertor::toDto).collect(Collectors.toList());
        dto.setQuestionAnswer(questionDtos);

        List<UserAnswersDto> userAnswersDtos = entity.getUserAnswers().stream().map(UserAnswersConverter::toDto).collect(Collectors.toList());
        dto.setUserAnswers(userAnswersDtos);
        return dto;
    }

    public static QuestionEntity toEntity(QuestionDto dto) {
        QuestionEntity entity = new QuestionEntity();
        entity.setId(dto.getId());
        entity.setTitle(dto.getTitle());
        entity.setDescription(dto.getDescription());
        entity.setQuestionType(dto.getQuestionType());
        entity.setQuestionModifier(dto.getQuestionModifier());
        entity.setTimeout(dto.getTimeout());

        List<QuestionAnswerEntity> questionEntitys = dto.getQuestionAnswer()
                .stream()
                .map(QuestionAnswerConvertor::toEntity)
                .collect(Collectors.toList());
        entity.setQuestionAnswers(questionEntitys);

        List<UserAnswersEntity> userAnswersEntities = dto.getUserAnswers()
                .stream()
                .map(UserAnswersConverter::toEntity)
                .collect(Collectors.toList());
        entity.setUserAnswers(userAnswersEntities);
        return entity;
    }
}
