package com.epam.tlt.techtalk_quiz.domain.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserDto {

    private Long id;
    private String login;
    private Integer balance;
    private List<UserRoleDto> userRoles;
    private List<UserAnswersDto> userAnswers;
}
