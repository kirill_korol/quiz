package com.epam.tlt.techtalk_quiz.domain.converter;

import com.epam.tlt.techtalk_quiz.domain.dto.UserAnswersDto;
import com.epam.tlt.techtalk_quiz.domain.dto.UserDto;
import com.epam.tlt.techtalk_quiz.domain.dto.UserRoleDto;
import com.epam.tlt.techtalk_quiz.domain.entity.UserAnswersEntity;
import com.epam.tlt.techtalk_quiz.domain.entity.UserEntity;
import com.epam.tlt.techtalk_quiz.domain.entity.UserRoleEntity;

import java.util.List;
import java.util.stream.Collectors;

public class UserConverter {

    public static UserDto toDto(UserEntity entity) {
        UserDto dto = new UserDto();
        dto.setId(entity.getId());
        dto.setLogin(entity.getLogin());
        dto.setBalance(entity.getBalance());

        List<UserAnswersDto> usersAnswersDto = entity.getUserAnswers().stream().map(UserAnswersConverter::toDto).collect(Collectors.toList());
        dto.setUserAnswers(usersAnswersDto);

        List<UserRoleDto> userRoleDto = entity.getRoles().stream().map(UserRoleConverter::toDto).collect(Collectors.toList());
        dto.setUserRoles(userRoleDto);

        return dto;
    }

    public static UserEntity toEntity(UserDto dto) {
        UserEntity entity = new UserEntity();
        entity.setId(dto.getId());
        entity.setLogin(dto.getLogin());
        entity.setBalance(dto.getBalance());

        List<UserAnswersEntity> usersAnswersEntities = dto.getUserAnswers()
                .stream()
                .map(UserAnswersConverter::toEntity)
                .collect(Collectors.toList());
        entity.setUserAnswers(usersAnswersEntities);

        List<UserRoleEntity> userRoleEntities = dto.getUserRoles()
                .stream()
                .map(UserRoleConverter::toEntity)
                .collect(Collectors.toList());
        entity.setRoles(userRoleEntities);
        return entity;
    }
}
