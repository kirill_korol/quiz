package com.epam.tlt.techtalk_quiz.domain.dto;

import lombok.Data;

@Data
public class QuestionAnswerDto {

    private Long id;
    private QuestionDto questionId;
    private String answerText;
    private boolean isCorrect;
}
