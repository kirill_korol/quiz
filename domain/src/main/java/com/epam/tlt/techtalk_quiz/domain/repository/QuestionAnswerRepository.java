package com.epam.tlt.techtalk_quiz.domain.repository;

import com.epam.tlt.techtalk_quiz.domain.entity.QuestionAnswerEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionAnswerRepository extends CrudRepository<QuestionAnswerEntity, Long> {
}
