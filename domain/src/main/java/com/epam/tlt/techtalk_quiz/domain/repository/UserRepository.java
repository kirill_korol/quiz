package com.epam.tlt.techtalk_quiz.domain.repository;

import com.epam.tlt.techtalk_quiz.domain.entity.UserEntity;
import com.epam.tlt.techtalk_quiz.domain.entity.UserRoleEntity;
import com.epam.tlt.techtalk_quiz.domain.entity.enums.UserRoleEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {
    boolean existsByLogin(String name);

    boolean existsByLoginAndPassword(String name, String password);

    @Transactional
    default UserEntity createUser(String name, String password) {
        UserEntity user = new UserEntity(name, password);
        user.setRoles(new ArrayList<UserRoleEntity>() {{
            new UserRoleEntity(UserRoleEnum.ROLE_USER);
        }});
        return save(user);
    }

    UserRepository findByLogin(String name);
}
