package com.epam.tlt.techtalk_quiz.domain.dto;

import lombok.Data;

@Data
public class SomeDto {
    private Long id;
    private String some;
}
