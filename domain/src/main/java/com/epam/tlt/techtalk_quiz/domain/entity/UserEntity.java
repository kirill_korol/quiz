package com.epam.tlt.techtalk_quiz.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "user")
public class UserEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "balance")
    private Integer balance;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user",cascade = {CascadeType.ALL})
    private List<UserRoleEntity> roles;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<UserAnswersEntity> userAnswers;


    public UserEntity(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public UserEntity(String login, String password, ArrayList<UserRoleEntity> userRoles) {
        this.login = login;
        this.password = password;
        this.roles = userRoles;
    }
}
