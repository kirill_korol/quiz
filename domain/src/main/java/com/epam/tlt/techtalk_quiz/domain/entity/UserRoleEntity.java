package com.epam.tlt.techtalk_quiz.domain.entity;

import com.epam.tlt.techtalk_quiz.domain.entity.enums.UserRoleEnum;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "user_role")
public class UserRoleEntity implements GrantedAuthority {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;

    @Column(name = "role")
    private String role;

    public UserRoleEntity(UserRoleEnum userRole) {
        this.role = userRole.toString();
    }

    @Override
    public String getAuthority() {
        return role;
    }
}
